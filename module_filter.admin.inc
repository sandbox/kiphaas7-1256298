<?php
/**
 * Settings form for Module filter.
 */
function module_filter_settings() {
  $form['tabs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tabs'),
    '#description' => t('Settings used with the tabs view of the modules page.'),
  );
  $form['tabs']['module_filter_all_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('\'All\' tab'),
    '#description' => t('Add a tab which holds all the modules, sorted alphabetically.'),
    '#default_value' => MODULE_ALL_TAB,
  );
  $form['tabs']['module_filter_history_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('History entry for each tab selection'),
    '#description' => t('Create an entry in the history of the browser every time a switch to a different tab occurs. This makes it possible to use the back/forward buttons of the browser to traverse between tab switches.'),
    '#default_value' => MODULE_HISTORY,
  );
  $form['tabs']['module_filter_count_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Number of enabled modules'),
    '#description' => t('Display the number of enabled modules and the total number number of modules in the active tab, along with a list of the enabled modules.'),
    '#default_value' => MODULE_COUNT,
  );
  $form['tabs']['module_filter_hide_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide tabs'),
    '#description' => t('Hide tabs which have no search matches.'),
    '#default_value' => MODULE_HIDE_TAB,
  );

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search field'),
    '#description' => t('Settings used with the Instant filter search field of the modules page.'),
  );
  $form['search']['module_filter_autocomplete_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autocomplete'),
    '#description' => t('Add an autocomplete feature to the search field.'),
    '#default_value' => MODULE_AUTOCOMPLETE,
  );

  return system_settings_form($form);
}
