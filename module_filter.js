(function($) {

/**
 * Attaches the moduleFilter behavior.
 */
Drupal.behaviors.moduleFilter = {
  attach: function (context, settings) {
    var element = $('#system-modules', context);
    element.once('moduleFilter', function () {
      element.data('modulefilter', new Drupal.moduleFilter(element, context, settings));
    });
  }
};

/**
 * The moduleFilter object.
 * 
 * @constructor
 * @param element
 *   DOM input element to attach the moduleFilter to.
 * @param context
 *   Context that possibly limits a DOM lookup.
 */
Drupal.moduleFilter = function (element, context, settings) {
  var self = this;
  this.element = element;
  this.context = context;
  this.settings = settings;

  // Set the parent element and an initial object containing a cache of the
  // vertical tabs,fieldsets and modules.
  this.setCache();

  // Move the submit button to the end of the tab list for a better UX.
  var submit = this.element.find('#edit-actions').wrap('<li class="module-filter-submit"></li>').parent();
  this.$tabs.filter(':last').after(submit);

  // Move the Instant filter input field and checkboxes in the vertical tabs
  // pane for a better UX.
  var wrapper = this.element.find('#edit-filterwrapper');
  this.element.find('.vertical-tabs-panes').prepend(wrapper);

  // Move the Instant filter 'no results' message after the checkboxes.
  this.element.find('#edit-show').after(this.element.find('.instantfilter-no-results'));

  // Bind a click event handler for updating the cache and tab summaries.
  this.element.bind('click', {self: self}, self.eventHandler);

  // Bind an Instant filter event in order to correctly reflect the change
  // in choices made by Instant filter (in conjunction with vertical tabs).
  this.$instantFilter.bind('drupalInstantFilterAfterSearch', {self: self}, self.InstantFilterTrigger);

  // Bind a click event handler to force tab <-> fieldset visibility.
  this.$tabs.children('a').bind('click', {self: self}, self.verticalTabsTrigger);

  // Add settings for the 'All' tab.
  if (this.settings.modulefilter.enableAll) {
    this.allTab();
  }

  // Bind the hashchange event.
  if (this.settings.modulefilter.enableHistory) {
    this.history();
  }

  // Add the jQuery UI autocomplete widget.
  if (this.settings.modulefilter.enableAutocomplete) {
    this.autocomplete();
  }
};

/**
 * Sets the cache.
 */
Drupal.moduleFilter.prototype.setCache = function () {
  var self = this;
  this.$tabs = this.element.find('ul.vertical-tabs-list li');
  this.currentTab = self.$tabs.index('.selected');
  this.$fieldsets = this.element.find('fieldset');
  this.$instantFilter = this.element.find('#edit-modulesfilter');

  this.filterRulesInit = {};
  this.filterRules = {};
  this.modulesMap = [];
  this.modules = {};
  this.tabs = [];

  // Get the initial status of each tab.
  this.$fieldsets.each(function (i) {
    var $fieldset = $(this);
    var $tab = self.$tabs.eq(i);

    self.tabs[i] = {
      warningShown: false,
      modulesAscLength: 0,
      modulesAsc: [],
      iniEnabled: null,
      enabled: null,
      searchMatches: null,
      total: null,
      $tab: $tab,
      $el: $tab.find('a > strong'),
      $fieldset: $fieldset
    };

    $fieldset.find(':checkbox').each(function (j) {
      var $this = $(this);
      var id = $this.attr('id');
      var state = $this.is(':checked');

      self.lastModule = id;
      self.modulesMap.push(id);
      self.tabs[i].modulesAscLength++;
      self.tabs[i].modulesAsc.push(id);
      self.modules[id] = {
        warningShown: false,
        status: state,
        iniStatus: state,
        disabled: $this.is(':disabled'),
        $el: $this.closest('tr'),
        name: $this.closest('td').next().text(),
        tabId: i
      };
    });

    self.fieldsetEnabled(true);
    self.fieldsetSummary();
  });

  // Minor performance improvement: cache lengths.
  this.tabsLength = this.tabs.length;
  this.modulesLength = this.modulesMap.length;
  

  // Save the initial status of the filters.
  this.filterActive = false;
  this.$instantFilter.closest('#edit-filterwrapper').find(':checkbox').each(function () {
    var $this = $(this);
    self.filterRulesInit[$this.attr('id')] = $this.is(':checked');
    self.filterRules[$this.attr('id')] = $this.is(':checked');
  });
};

/**
 * Stores the names of the enabled modules.
 */
Drupal.moduleFilter.prototype.fieldsetEnabled = function (init) {
  var lastModule = this.modules[this.lastModule];
  var tab = this.tabs[lastModule.tabId];

  // Filter out the enabled modules.
  for (var i = 0, j = 0, enabled = []; i < tab.modulesAscLength; i++) {
    var module = this.modules[tab.modulesAsc[i]];
    if (module.status) {
      enabled[j] = module.name;
      j++;
    }
  }

  tab.enabled = enabled;
  if (init) {
    tab.iniEnabled = enabled;
    tab.total = i;
  }
};

/**
 * Creates a summary to be used in vertical tabs.
 */
Drupal.moduleFilter.prototype.fieldsetSummary = function () {
  var lastModule = this.modules[this.lastModule];
  var tab = this.tabs[lastModule.tabId];
  var self = this;

  tab.$fieldset.drupalSetSummary(Drupal.theme('moduleFilterEnabled', tab, self.settings));
};

/**
 * Event handler triggered by enabling/disabling checkboxes.
 */
Drupal.moduleFilter.prototype.eventHandler = function (event) {
  var self = event.data.self;
  var target = event.target;
  var $target = $(target);
  var id = $target.attr('id');

  // Module filter supplied filters for Instant filter.
  if (target.nodeName === 'INPUT' && target.type === 'checkbox') {
    if ($target.hasClass('modulefilter-filter')) {
      self.filterActive = false;
      self.filterRules[id] = $target.is(':checked');
      for (id in self.filterRulesInit) {
        if (self.filterRulesInit[id] !== self.filterRules[id]) {
          self.filterActive = true;
          break;
        }
      }
      self.$instantFilter.trigger('drupalInstantFilterTriggerSearch');
    }

    // 'Enabled' module checkbox.
    else {
      var module = self.modules[id];
      var tab = self.tabs[module.tabId];

      // Update module status, last module and enabled modules.
      module.status = $target.is(':checked');
      self.lastModule = id;
      self.fieldsetEnabled(false);

      // Show warning sign near tab name if necessary.
      var iniEnabledConcat = tab.iniEnabled.join(',');
      var enabledConcat = tab.enabled.join(',');
      if (!tab.warningShown && iniEnabledConcat !== enabledConcat) {
        tab.$el.append(Drupal.theme('moduleFilterChangedMarker'));
        tab.warningShown = true;
      }
      else if (tab.warningShown && iniEnabledConcat === enabledConcat) {
        tab.$el.find('.module-changed').remove();
        tab.warningShown = false;
      }

      // Add a CSS class indicating a status change.
      if (!module.warningShown && module.iniStatus !== module.status) {
        var changed = module.status ? 'module-enabled' : 'module-disabled';
        module.$el.addClass(changed);
        module.warningShown = true;
      }
      else if (module.warningShown && module.iniStatus === module.status) {
        module.$el.removeClass('module-enabled module-disabled');
        module.warningShown = false;
      }

      // Toggle class.
      if (self.settings.modulefilter.hideTabs) {
        self.toggleTab(true);
      }

      // Update tab summary.
      self.fieldsetSummary();
    }
  }
};

/**
 * Determines if Instant filter has a search going on.
 */
Drupal.moduleFilter.prototype.hasSearch = function () {
  // One or more active filters equals an active search.
  if (this.filterActive) {
    return true;
  }

  // Check if Instant filter has a valid search parameter.
  var data = this.$instantFilter.data('instantfilter');
  return (typeof data.search !== 'undefined' && $.trim(data.search) !== '') ? true : false;
};

/**
 * Counts searchmatches for each tab.
 */
Drupal.moduleFilter.prototype.countSearchMatches = function () {
  var settings = this.settings.modulefilter;

  // Reset searchmatches.
  this.globalMatches = [];
  this.tabs.AnyMatch = 0;
  for (var i = 0; i < this.tabsLength; i++) {
    this.tabs[i].searchMatches = null;
  }

  // Update the searchmatch counts if there is a search ongoing.
  if (this.searchActive) {
    for (var i = 0; i < this.modulesLength; i++) {
      var id = this.modulesMap[i];
      var match = this.modules[id].$el.hasClass('instantfilter-item-no-match') ? false : true;
      var tab = this.tabs[this.modules[id].tabId];

      if (tab.searchMatches == null) {
        tab.searchMatches = 0;
      }

      if (match) {
        // Do not include the 'All' tab results (duplication).
        if (!settings.enableAll || (settings.enableAll && this.modules[id].tabId !== this.allTab)) {
          this.globalMatches[this.tabs.AnyMatch] = this.modules[id].name;
          this.tabs.AnyMatch++;
        }
        tab.searchMatches++;
      }
    }
  }

  // Update every tab summary.
  var lastModule = this.lastModule;
  for (var i = 0; i < this.tabsLength; i++) {
    this.lastModule = this.tabs[i].modulesAsc[0];
    this.fieldsetSummary();

    // Toggle class.
    if (this.settings.modulefilter.hideTabs) {
      this.toggleTab(i === (this.tabsLength - 1));
    }
  }
  this.lastModule = lastModule;
}

/**
 * Handler which toggles a CSS class on a tab in case of no search results.
 */
Drupal.moduleFilter.prototype.toggleTab = function (loopFinished) {
  var lastModule = this.modules[this.lastModule];
  var tab = this.tabs[lastModule.tabId];

  // Toggle the 'no-matches' CSS class also on the tab itself.
  var searchActive = (typeof this.searchActive === 'undefined') ? false : true;
  var searchMatches = (tab.searchMatches === 0) ? false : true;
  tab.$tab.toggleClass('no-matches', (searchActive && !searchMatches));

  if (loopFinished) {
    // Fix first/last class due to the potential hiding.
    this.$tabs.removeClass('first last').filter(':visible')
      .filter(':first').addClass('first').end()
      .filter(':last').addClass('last');
  }
};

/**
 * Handler which makes sure that Module filter and Instant filter together
 * produce a consistent module listing.
 */
Drupal.moduleFilter.prototype.InstantFilterTrigger = function (event) {
  var self = event.data.self;
  var settings = self.settings.modulefilter;

  // Boolean indicating if there is an actual search.
  self.searchActive = self.hasSearch();

  // Count search matches.
  self.countSearchMatches();
  var currentTabMatches =  self.tabs[self.currentTab].searchMatches;

  // Return early if there is a search with no results, because Instant filter
  // has already hidden all the fieldsets.
  if (self.searchActive && !currentTabMatches && !self.tabs.AnyMatch) {
    // Instant filter has hidden all the fieldsets, but vertical tabs insists
    // on showing the tab <-> fieldset pair.
    self.tabs[self.currentTab].$fieldset.hide();
    return;
  }

  // Enforce current tab-fieldset visibility. This is necessary because
  // Instant filter toggles all fieldsets based on if they have matches or not.
  // This module uses vertical tabs, so only a single tab and fieldset should
  // be selected/visible.
  self.$fieldsets.hide().eq(self.currentTab).show();

  // If there is globally at least one match and the current open tab does not
  // have any matches, switch to the tab which has the most matches.
  if (self.tabs.AnyMatch && !currentTabMatches) {
    var mostMatches = {count: 0, tabId: 0};
    for (var i = 0; i < self.tabsLength; i++) {
      // Never switch to the 'All' tab.
      if (!settings.enableAll || (settings.enableAll && i !== self.allTab)) {
        // If there are multiple tabs which have the same amount of matches, it
        // seems intuitively more logical to select the first tab; seen top
        // to bottom. Therefore use '>' instead of '>='.
        if (self.tabs[i].searchMatches > mostMatches.count) {
          mostMatches.count = self.tabs[i].searchMatches;
          mostMatches.tabId = i;
        }
      }
    }
    // Trigger a click on the tab with the highest number of matches.
    self.tabs[mostMatches.tabId].$tab.children('a').click();
  }
};

/**
 * Handler which updates the active tab.
 */
Drupal.moduleFilter.prototype.verticalTabsTrigger = function (event) {
  // Update current tab number. The method used in setCache() can't be used
  // because the 'selected' class is not yet updated by vertical tabs when
  // triggered.
  var self = event.data.self;
  var $clickedTab = $(this).closest('li');
  self.currentTab = self.$tabs.index($clickedTab);

  // Create a history entry.
  if (self.settings.modulefilter.enableHistory) {
    var state = {};
    state[self.history.stateId] = self.history.stateCacheHash[self.currentTab];
    $.bbq.pushState(state);
  }
};

/**
 * History handler.
 */
Drupal.moduleFilter.prototype.history = function (event) {
  var self = this;

  this.history = {
    stateId: 'module-tab',
    defaultTab: 0,
    stateCacheHash: [],
    stateCacheEq: {}
  };

  for (i = 0; i < this.tabsLength; i++) {
    var id = this.tabs[i].$fieldset.attr('id').replace('edit-modules-', '');
    this.history.stateCacheHash[i] = id;
    this.history.stateCacheEq[id] = i;
  }
  this.history.stateCacheEq[''] = this.history.defaultTab;

  $(window).bind('hashchange', {self: self}, self.historyTrigger);
  $(window).trigger('hashchange');
};

/**
 * Handler which choses the active tab based on a hash.
 */
Drupal.moduleFilter.prototype.historyTrigger = function (event) {
  var self = event.data.self;
  var hist = self.history;

  // Get the current url from the hashtag.
  var url = event.getState(self.stateId);
  if (!url[hist.stateId]) {
    url = {};
    url[hist.stateId] = '';
  }

  // If the current url equals the previous url, return early.
  if (hist.lastUrl && hist.lastUrl[hist.stateId] === url[hist.stateId]) {
    return;
  }

  // Save the current url.
  hist.lastUrl = url;

  // Trigger a click on the corresponding tab.
  var eq = hist.stateCacheEq[url[hist.stateId]];
  if (typeof eq === 'undefined') {
    eq = self.history.defaultTab;
  }
  if (eq !== self.currentTab) {
    self.tabs[eq].$tab.children('a').focus().click();
  }
};

/**
 * Handler for the 'All' tab.
 */
Drupal.moduleFilter.prototype.allTab = function () {
  this.allTab = 0;
  for (var i = 0; i < this.tabsLength; i++) {
    if (this.tabs[i].$el.text() === this.settings.modulefilter.allTabName) {
      this.allTab = i;
      break;
    }
  }
};

/**
 * Autocomplete handler for the Instant filter field.
 */
Drupal.moduleFilter.prototype.autocomplete = function () {
  var self = this;

  this.$instantFilter.autocomplete({
    minLength: 3,
    source: function (request, response) { response(self.globalMatches); }
  });

  // Autocomplete sets the 'autocomplete' attribute to off; this resets the
  // input field each time the page is visited from cache using the back or
  // forward browser buttons. This leads to weird situations, because the old
  // search results are still visible.
  // http://www.w3.org/TR/html5/common-input-element-attributes.html
  this.$instantFilter.attr('autocomplete', 'on');

  // However, now the browser autocomplete could kick in. To prevent any
  // previously entered values to pop up on top of the input field, and
  // possibly overlaying the jQuery UI autocomplete, generate a unique name
  // on each page load to prevent the browser from associating the previously
  // entered values with the input field.
  var oldname = this.$instantFilter.attr('name');
  var time = new Date().getTime();
  this.$instantFilter.attr('name', oldname + time);

  // Instant filter does not listen to the 'change' event by default.
  // This is the only (non jQuery UI) event triggered by the autocomplete
  // widget if a selection is made by mouse. Also, the search event by
  // Instant filter needs to be delayed a bit, otherwise it does not use the
  // newly selected value.
  this.$instantFilter.bind('change', function() {
    setTimeout(function () {
      var data = self.$instantFilter.data('instantfilter');
      var value = self.$instantFilter.val().toLowerCase();
      if (typeof data.search !== 'undefined' && (value !== data.search)) {
        self.$instantFilter.trigger('drupalInstantFilterTriggerSearch');
      }
    }, 250);
  });
}

/**
 * Filter rules for Instant filter.
 */
Drupal.instantFilter.hooks.alterMatch.moduleFilterVisible = {
  rule: function (instantfilter, item, i) {
    // Get the data from our own moduleFilter.
    var data = instantfilter.container.data('modulefilter');
    var module = data.modules[data.modulesMap[i]];

    // Show enabled modules.
    if (data.filterRules['edit-show-enabled']) {
      if (module.status && !module.disabled) {
        return true;
      }
    }

    // Show disabled modules.
    if (data.filterRules['edit-show-disabled']) {
      if (!module.status && !module.disabled) {
        return true;
      }
    }

    // Show enabled modules that are required by other enabled modules.
    if (data.filterRules['edit-show-required']) {
      if (module.status && module.disabled) {
        return true;
      }
    }

    // Show disabled modules that need other modules to be enabled first.
    if (data.filterRules['edit-show-unavailable']) {
      if (!module.status && module.disabled) {
        return true;
      }
    }

    return false;
  }
};

/**
 * Theme function for a tab summary which contains the number of enabled
 * modules, total number of modules, a list of enabled module names and a search
 * count.
 */
Drupal.theme.prototype.moduleFilterEnabled = function (tab, settings) {
  if (settings.modulefilter.enableCount) {
    var count = Drupal.t('@count/@total modules enabled:', {'@count': tab.enabled.length, '@total': tab.total});
    var list = Drupal.checkPlain(tab.enabled.join(', '));
    count = '<strong class="module-fieldset-count">' + count + ' </strong>';
    list = '<span class="module-fieldset-list">' + list + '</span>';
    var returnValue = count + list;
  }
  else {
    var returnValue = '';
  }

  if (tab.searchMatches != null) {
    var searchClasses = 'module-fieldset-matches ' + (tab.searchMatches ? ' has-matches' : ' no-matches');
    var searchCount = Drupal.t('@count/@total', {'@count': tab.searchMatches, '@total': tab.total});
    returnValue += '<em class="' + searchClasses + '">' + searchCount + '</em>';
  }

  return returnValue;
};

/**
 * Theme function which indicates a tab contains modules that have been disabled
 * or enabled by the user.
 */
Drupal.theme.prototype.moduleFilterChangedMarker = function () {
  return '<span class="warning module-changed">*</span>';
};

})(jQuery);